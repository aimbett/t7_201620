package taller.test;

import junit.framework.TestCase;
import taller.ArbolBinario;

public class MainTest extends TestCase{

	public void testSample() {
		assertEquals(2, 2);
	}

	public void test1(){
		ArbolBinario<String> arbol;
		String[] inorden = { "d","b","e","a","f","c","g" };
		String[] preorden = { "a","b","d","e","c","f","g" };

		arbol = new ArbolBinario<>(inorden,preorden);

		assertEquals("a", arbol.raiz.val);
		assertEquals("c", arbol.raiz.derecho.val);
		assertEquals("b", arbol.raiz.izquierdo.val);
		assertEquals("e", arbol.raiz.izquierdo.derecho.val);
	}

	public void test2(){
		String[] inorden = { "k","l","j","h","q","o","p" };
		String[] preorden = { "j","k","l","o","q","h","p" };

		ArbolBinario<String> arbol = new ArbolBinario<>(inorden,preorden);


		assertEquals("j", arbol.raiz.val);
		assertEquals("o", arbol.raiz.derecho.val);
		assertEquals("k", arbol.raiz.izquierdo.val);
		assertEquals("l", arbol.raiz.izquierdo.derecho.val);
	}




}