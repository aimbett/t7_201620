package taller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import taller.interfaz.IReconstructorArbol;

public class ArbolBinario<T extends Comparable<T>> implements IReconstructorArbol {
	public class Nodo<T extends Comparable<T>> implements Comparable {

		public T val;           
		public Nodo izquierdo, derecho;   
		
		public Nodo( T val) {
			this.val = val;
		}
		
		
		public int compareTo(Object arg0) {
			// TODO Auto-generated method stub
			T param = (T) arg0;
			return val.compareTo(param);
		}
	}
	
	
	public Nodo raiz;
	private int preIndice ;
	private T in[];
	private T pre[];
	
	 /**
     * Initializes an empty symbol table.
	 * @throws Exception 
     */
    public ArbolBinario(String file) throws Exception {
    	preIndice = 0;
    	cargarArchivo(file);
    	reconstruir();
    	System.out.println("Arbol en Inorden");
    	imprimirInOrden(raiz);
    	//imprimirJSon();
    	//archivoJson();
    	crearArchivo(null);
    	System.out.println(imprimirInOrden(raiz));
    }
    
    
    public ArbolBinario(T inOrden[], T preOrden[])
    {
    	preIndice = 0;
    	in = inOrden;
    	pre=preOrden;
    	reconstruir();
    }

    public void agregar(T valor)
    {
    	agregar(valor, raiz);
    }
    public boolean agregar(T valor, Nodo nod)
    {
    	boolean resp = false;
    	if(nod == null){
    		nod = new Nodo(valor);
    		resp = true;
    	}
    	else{
    		resp = nod.val.compareTo(valor)<0 ? agregar(valor, nod.izquierdo) : agregar(valor, nod.derecho);
    	}
    	
    	return resp;
    }
    
   
 public Nodo construirArbol(T inOrden[], T preOrden[], int inicioInOrden, int finInOrden) 
 {
     if (inicioInOrden > finInOrden) 
         return null;

     
     Nodo tNode = new Nodo(preOrden[preIndice++]);

     
     if (inicioInOrden == finInOrden)
         return tNode;

     
     int inIndex = indiceArreglo(inOrden, inicioInOrden, finInOrden, tNode.val);

    
     tNode.izquierdo = construirArbol(inOrden, preOrden, inicioInOrden, inIndex - 1);
     tNode.derecho = construirArbol(inOrden, preOrden, inIndex + 1, finInOrden);

     return tNode;
 }
 
 
 public int indiceArreglo(T arr[], int inicio, int fin, Comparable value) 
 {
     int i;
     for (i = inicio; i <= fin; i++) 
     {
         if (value.compareTo(arr[i])==0)
             return i;
     }
     return i;
 }
 
 
 public String imprimirInOrden(Nodo node) 
 {
	 String resp ="";
     if (node == null)
         return"";

     
    resp+= imprimirInOrden(node.izquierdo);

    
     resp += node.val + ",";

     
     resp+= imprimirInOrden(node.derecho);
     
     return resp;
 }
 
 public void archivoJson()
 {
	 try{
		    PrintWriter writer = new PrintWriter("./data/arbolPlantado.json");
		   
		    writer.println("{");
			 writer.println("\"Nodos\" : {");
			 writer.println("\"Nodo\" : [");
			 writer.println(JsonNodo(raiz));
			 writer.println("\n ] \n } \n }");
		    writer.close();
		} catch (Exception e) {
		   // do something
		}
 }
 public void imprimirJSon()
 {
	 System.out.println("{");
	 System.out.println("\"Nodos\" : {");
	 System.out.println("\"Nodo\" : [");
	 System.out.println(JsonNodo(raiz));
	 System.out.println("\n ] \n } \n }");
	 
 }
 
 public String JsonNodo(Nodo nodo)
 {
	 String resp="";
	 if(nodo == null)
	 {
		 return"";
	 }
	 else{
		 resp +="{\n";
		 
		 resp +="\"Actual\":\"" + nodo.val+"\",\n";
		 
		 resp += nodo.izquierdo != null ? "\"Izquierdo\":\"" + nodo.izquierdo.val+"\",\n" : "\"Izquierdo\":\"" + "Sin hijo\",\n";

		 resp += nodo.derecho != null ? "\"Derecho\":\"" + nodo.derecho.val+"\"\n" : "\"Derecho\":\"" +"Sin hijo\"\n";
		 resp+= "}";
		 if(nodo.izquierdo!= null)
			 {
			 	resp+= ",\n";
			 	 resp += JsonNodo(nodo.izquierdo);
			 }
		if(nodo.derecho!= null)
		{
			resp+= ",\n";
			resp += JsonNodo(nodo.derecho);
		}
		
	 }
	 return resp;
 }

@Override
public void cargarArchivo(String nombre) throws IOException {
	// TODO Auto-generated method stub
	Properties propiedades = new Properties();
	FileInputStream fileInputStream = new FileInputStream(nombre);
	propiedades.load(fileInputStream);
	fileInputStream.close();
	
	String preOrden = propiedades.getProperty("preorden");
	String inOrden = propiedades.getProperty("inorden");
	
	System.out.println(preOrden);
	System.out.println(inOrden);
	
	String[] arregloPre = preOrden.split(",");
	String[] arregloIn = inOrden.split(",");
	in = (T[]) arregloIn;
	pre =(T[]) arregloPre;
	
}

@Override
public void crearArchivo(String info) throws FileNotFoundException, UnsupportedEncodingException {
	// TODO Auto-generated method stub
	archivoJson();
	
}

@Override
public void reconstruir() {
	// TODO Auto-generated method stub
	raiz = construirArbol(in, pre, 0, in.length - 1);
	
}
 
public boolean contieneSubArbol(Nodo nActual, String inOrden)
{
	boolean actual = false;
	if(nActual== null)
	{
		return false;
	}
	String inOrdenAcual = imprimirInOrden(nActual);
	if(inOrdenAcual.equals(inOrden+";"))
	{
		actual = true;
	}
	else{
			actual = contieneSubArbol(nActual.izquierdo, inOrden);
			if(!actual)
			{
				actual = contieneSubArbol(nActual.derecho, inOrden);
			}
	}
	
	return actual;
	
}
 
}
